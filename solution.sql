-- 1. find all artists that has letter "d" in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- 2. find all songs that has a length of less than 3:50
SELECT * FROM songs WHERE length < 350;

-- 3. join albums and songs table (only show the album name, song name, and song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- 4. join the artists and albums tables(find all albums that has letter "a" in its name).
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE albums.album_title LIKE "%a%";

-- 5. sort the albums in Z-A order(show only the first 4 records).
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- 6. join the albums and songs tables(sort the albums from Z-A)
SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY album_title DESC, song_name ASC;